# EntropyCalc

## Introduction
EntropyCalc is set of specialized tools providing statistical analysis of nucleotide sequences. It is written in Java and uses Maven as its build system.
It is divided into three modules:
* EntropyCalcWeb - A web application built around the Spring MVC framework and deployed on Apache Tomcat. This is the preffered application for users.
* EntropyCalcCLI - A CLI application. This is historic project and it is no longer actively mantained and supports only minimal functionality.
* EntropyCalcLib - A library containing the statistical code used by the above mentioned applications.


## EntropyCalcWeb
EntropyCalcWeb provides users following functions:
* PNNS Calculator
* Entropy Calculator
* Sequence Tracer

### PNNS Calculator
The PNNS (positional nucleotide numerical summary) calculator summarises the characters in each column of the multiple sequence alignment.
The output is listed in columns. The first column designates the position number, columns *A*, *G*, *C* and *T* counts the number of nucleotides per given positions. The *GAP* summarises missing data or gaps and *Misc* indicates woble positions like Y, R, N etc. The *SUM* column calculates the informative data (*A*+*C*+*G*+*T*) per position and the *Total* column = *SUM*+*GAP*+*Misc*.

The output example of the PNNS calculator is shown below:

|Position       |A      |G      |C      |T      |GAP    |Misc   |SUM    |Total|
|---:            |---:    |---:    |---:    |---:    |---:    |---:    |---:    |---:  |
|1              |2      |21083  |1      |4      |440    |0      |21090  |21530|
|2              |2      |21098  |1      |1      |428    |0      |21102  |21530|
|3              |1      |3      |21097  |3      |425    |1      |21104  |21530|
|4              |1      |0      |21119  |0      |410    |0      |21120  |21530|
|5              |2      |0      |21126  |2      |400    |0      |21130  |21530|
|6              |0      |4      |21127  |1      |398    |0      |21132  |21530|
|7              |4      |3      |21174  |8      |340    |1      |21189  |21530|
|8              |3      |1      |5      |21183  |338    |0      |21192  |21530|
|9              |1      |4      |21186  |4      |335    |0      |21195  |21530|
|10             |21188  |1      |4      |1      |336    |0      |21194  |21530|
|11             |21194  |1      |1      |0      |334    |0      |21196  |21530|
|12             |21185  |13     |0      |0      |332    |0      |21198  |21530|
|13             |7      |21191  |0      |1      |331    |0      |21199  |21530|
|14             |2      |3      |21190  |1      |333    |1      |21196  |21530|
|15             |2      |4      |21188  |5      |331    |0      |21199  |21530|
|16             |3      |21191  |5      |0      |329    |2      |21199  |21530|
|17             |21200  |2      |0      |0      |328    |0      |21202  |21530|
|18             |1      |21324  |1      |0      |203    |1      |21326  |21530|
|19             |0      |4      |1      |21322  |202    |1      |21327  |21530|
|20             |2      |21323  |0      |2      |202    |1      |21327  |21530|

For additional information please refer to:  
*Nagy, A., Jiřinec, T., Černíková, L., Jiřincová, H. & Havlíčková, M. Large-scale nucleotide sequence alignment and sequence variability assessment to identify the evolutionarily highly conserved regions for universal screening PCR assay design: An example of influenza A virus. Methods Mol. Biol. 1275, 57-72 (2015).*

### Entropy Calculator
The *Entropy Calculator* quantifies the amount of variability through each column of a multiple nucleic acid sequence alignment.
The information entropy is a measure of uncertainty. Applying this approach to a large-scale sequence alignment means quantifying of the amount of variability in each position according to the formula:

```math
H(i) = -\sum_i{P(x,i) \cdot log_2 P(x,i)} = -\sum_i{P(x,i) \cdot ln P(x,i)}
```

where *H(i)* is the entropy at position *i*, *P(x,i)* is the probability of each base *x* at position *i* in a multiple sequence alignment. Accordingly, the most variable positions have the highest entropy values (the lowest information content and therefore the highest uncertainty). Conversely, the conserved positions are those with the lowest entropy values (the highest information content) converging or equal to zero.

The input for the *Entropy Calculator* is a positional nucleotide numerical summary PNNS. This summary is then converted to entropy values by the *Entropy Calculator*.  
Its output is an excel sheet with the following columns:

**Position	A	G	C	U	GAP	SUM	Total	Afreq	Gfreq	Cfreq	Ufreq	lnAfreq	lnGfreq	lnCfreq	lnUfreq	H(A)	H(G)	H(C)	H(U)	H(i)	%Coverage**

The firts 8 columns (from *Position* to *Total* is identical with the PNNS calculator output. The columns *Afreq* through *H(U)* shows the calculation chain. The *H(i)* represents the entropy values and the *%Coverage* expressess the amount of informative data (coverage) per position.

For additional information please refer to:  
*Nagy, A., Jiřinec, T., Černíková, L., Jiřincová, H. & Havlíčková, M. Large-scale nucleotide sequence alignment and sequence variability assessment to identify the evolutionarily highly conserved regions for universal screening PCR assay design: An example of influenza A virus. Methods Mol. Biol. 1275, 57-72 (2015).*

### Sequence Tracer
*Sequence Tracer* divides the LS-MSA, or the selected portions of it, into discrete groups of identical sequence variants. Then, the sequence count in each group and the group frequency percentage are calculated. The groups are numbered and listed in descending order. In parallel with sequence stratification, a graphical output in the form of a column diagram, which represents the frequencies of the particular variants as a function of the group number, is prepared. As a result, the *Sequence Tracer* provides a quick overview of the sequence variant composition of the LS-MSA along with its frequencies.

Once the LS-MSA is deconvoluted into its constituent elements, the *Sequence Tracer* provides group browsing and sequence sorting options. Each variant group can be further explored for specific IA virus strain content. Next, the particular sequences of interest or entire groups can be copied to the *Notes*, thus creating the user’s own personal subset. Finally, the content of the *Notes* can be exported as a FASTA file and browsed with the user’s preferred alignment editing software. The whole process of sequence tracing is fully intuitive, and the results are clearly interpretable without the necessity of extensive computational or biostatistical skills. Additional features are represented by the functions:
* Compress - prepares a weighted alignment by selecting a representative sequence of each variant group. Then, for each group, a weight (frequency) is assigned which reflects the abundance in the original LS-MSA.
* Stratify - calculates the number and frequency of each group with a graphical output. 
* Export informatives - exports all informative sequences, i.e. all sequence groups except outgroups 1, 2, and the “excluded” submissions.
