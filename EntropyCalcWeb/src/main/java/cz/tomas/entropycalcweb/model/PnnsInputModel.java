package cz.tomas.entropycalcweb.model;

import org.springframework.web.multipart.MultipartFile;

public class PnnsInputModel {

	private MultipartFile file;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
}
