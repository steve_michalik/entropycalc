package cz.tomas.entropycalcweb.model;

import cz.tomas.entropycalclib.Interval;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

public class SeqInputModel {

	private MultipartFile file;

//	@Min(1) @Max(100) @NotNull
//	private Integer threshold;

	@NotNull
	private Interval intervals[];

	private boolean useAll;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

//	public Integer getThreshold() {
//		return threshold;
//	}
//
//	public void setThreshold(Integer threshold) {
//		this.threshold = threshold;
//	}
	public boolean isUseAll() {
		return useAll;
	}

	public void setUseAll(boolean useAll) {
		this.useAll = useAll;
	}

	public Interval[] getIntervals() {
		return intervals;
	}

	public void setIntervals(Interval[] intervals) {
		this.intervals = intervals;
	}
}
