package cz.tomas.entropycalcweb.model;

import cz.tomas.entropycalclib.GroupedSequence;
import cz.tomas.entropycalclib.Interval;

import java.util.Collection;
import java.util.List;

/**
 * Created by Tomas on 18/12/2015.
 */
public class SeqBigResultModel {

    public long time;
    public int seqLength;
    public int seqTotal;

    public Collection<Interval> intervals;

    public List<GroupedSequence> groups;
}
