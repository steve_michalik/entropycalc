package cz.tomas.entropycalcweb.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.springframework.web.multipart.MultipartFile;

public class EntropyInputModel {

	private int precision;

	private boolean useSequences;

	@Min(10) @Max(100) @NotNull
	private Integer threshold;

	@Min(0) @Max(100) @NotNull
	private Integer startPosition;

	private MultipartFile file;

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public boolean isUseSequences() {
		return useSequences;
	}

	public void setUseSequences(boolean useSequences) {
		this.useSequences = useSequences;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	public Integer getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(Integer startPosition) {
		this.startPosition = startPosition;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public EntropyInputModel() {
		this.threshold = 95;
		this.startPosition = 1;
	}
}
