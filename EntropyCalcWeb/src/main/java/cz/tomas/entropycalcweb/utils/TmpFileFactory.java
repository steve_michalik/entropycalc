package cz.tomas.entropycalcweb.utils;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * Created by Tomas on 20/02/2017.
 */

@Component
public class TmpFileFactory {

    private final String RESULT_SUFFIX = ".tmp";
    private final Path TMP_PATH;

    public TmpFileFactory() {
        TMP_PATH = Paths.get(System.getProperty("java.io.tmpdir"));
    }

    public FileOutputStream getNewFile(UUID uuid) throws FileNotFoundException {
        return new FileOutputStream(getFile(uuid));
    }

    public File getFile(UUID uuid) {
        return TMP_PATH.resolve(uuid + RESULT_SUFFIX).toFile();
    }
}

