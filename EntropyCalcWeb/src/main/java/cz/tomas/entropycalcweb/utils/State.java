package cz.tomas.entropycalcweb.utils;

/**
 * Created by Tomas on 22/12/2015.
 */
public enum State {
    HOME,
    ENTROPY,
    SEQUENCE,
    PNNS,
    LINKS
}
