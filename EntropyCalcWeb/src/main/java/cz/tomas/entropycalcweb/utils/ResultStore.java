package cz.tomas.entropycalcweb.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Tomas on 18/12/2015.
 */
public class ResultStore<T> extends TimerTask {

    final Logger logger = LoggerFactory.getLogger(ResultStore.class);

    private class ValueItem<T>
    {
        public UUID uuid;
        public LocalDateTime deleteTime;
        public T value;

        public ValueItem(UUID uuid, T value, Duration persistence) {
            this.uuid = uuid;
            this.value = value;
            this.deleteTime = LocalDateTime.now().plus(persistence);
        }

        public ValueItem(UUID uuid, T value) {
            this(uuid, value, PERSISTENCE);
        }

        public void touch() {
            deleteTime = LocalDateTime.now().plus(PERSISTENCE);
        }

        public void markForDelete() {
            deleteTime = LocalDateTime.now().plus(PERSISTENCE_DELETED);
        }
    }

    private static final int MAX_SIZE = 100;
    private static final int CLEANUP_CHECK_PERIOD_MS = 20*1000;
    private static final Duration PERSISTENCE = Duration.ofMinutes(5);
    private static final Duration PERSISTENCE_DELETED = Duration.ofMinutes(1);

    private Map<UUID, ValueItem<T>> results = new HashMap<>();

    public ResultStore(Timer timer) {
        timer.scheduleAtFixedRate(this, 0, CLEANUP_CHECK_PERIOD_MS);
    }

    public synchronized void addResult(UUID uuid, T result) {
        logger.debug("added: {}", uuid);

        ValueItem<T> val = new ValueItem<T>(uuid, result);

        if (results.size() >= MAX_SIZE) {
            ValueItem<T> oldest = null;
            for (ValueItem<T> v : results.values()) {
                if (oldest == null) {
                    oldest = v;
                    continue;
                }

                if (v.deleteTime.isBefore(oldest.deleteTime)) {
                    oldest = v;
                }
            }

            results.remove(oldest.uuid);
            logger.debug("too many results, removed: {}", oldest.uuid);
        }

        results.put(uuid, val);
    }

    public synchronized T getResult(UUID uuid) {
        logger.debug("getting: {}", uuid);

        ValueItem<T> v = results.get(uuid);

        if (v != null) {
            v.touch();
            return v.value;
        }

        return null;
    }

    public synchronized boolean markForDelete(UUID uuid) {
        logger.debug("marking for delete: {}", uuid);

        ValueItem<T> v = results.get(uuid);

        if (v != null) {
            v.markForDelete();
            return true;
        }

        return false;
    }

    @Override
    public void run() {
        synchronized (this) {
            logger.trace("checking results, len: {}", results.size());

            List<ValueItem> toDelete = new ArrayList<>();

            LocalDateTime now = LocalDateTime.now();
            for (ValueItem<T> v : results.values()) {
                if (now.isAfter(v.deleteTime))
                {
                    toDelete.add(v);
                }
            }

            for (ValueItem<T> v : toDelete) {
                logger.debug("deleting result: {}", v.uuid);
                results.remove(v.uuid);
            }

            toDelete.clear();
        }
    }
}
