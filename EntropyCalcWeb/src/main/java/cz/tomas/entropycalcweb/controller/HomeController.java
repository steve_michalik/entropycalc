package cz.tomas.entropycalcweb.controller;

import cz.tomas.entropycalcweb.utils.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	final Logger logger = LoggerFactory.getLogger(HomeController.class);

	public HomeController() {
	}

	@RequestMapping(value = "/")
	public String home(Model model) {
		model.addAttribute("state", State.HOME);
		return "home";
	}

	@RequestMapping(value = "/links")
	public String links(Model model) {
		model.addAttribute("state", State.LINKS);
		return "links";
	}
}
