package cz.tomas.entropycalcweb.controller;

import cz.tomas.entropycalclib.Api;
import cz.tomas.entropycalcweb.model.SeqBigResultModel;
import cz.tomas.entropycalcweb.utils.ResultStore;
import cz.tomas.entropycalcweb.utils.State;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.UUID;

@Controller
public class SeqController {

    final Logger logger = LoggerFactory.getLogger(SeqController.class);

    @Autowired
    private ResultStore<SeqBigResultModel> resultStore;


    @RequestMapping(value = "/sequences", method = RequestMethod.GET)
    public String seqInput(Model model) {
        model.addAttribute("state", State.SEQUENCE);
        model.addAttribute("ctrl", "seqInputCtrl");
        return "sequences/dataInput";
    }

    @RequestMapping(value = "/sequences/{uuid}", method = RequestMethod.GET)
    public String sequences(Model model, @PathVariable("uuid") String uuid) {
        model.addAttribute("state", State.SEQUENCE);
        model.addAttribute("uuid", uuid);
        model.addAttribute("ctrl", "sequencesCtrl");
        return "sequences/result";
    }

    @RequestMapping(value = "/sequences/chart/{uuid}")
    public String getChart(@PathVariable("uuid") String uuid, HttpServletResponse response) throws Exception {

        logger.debug("chart result: {}", uuid);

        SeqBigResultModel result = resultStore.getResult(UUID.fromString(uuid));

        if (result == null) {
            return "entropy/noResult";
        }

        InputStream input;
        try {
            input = Api.createChart(result.groups);
        } catch (Exception e) {
            logger.error("chart creating error", e);
            throw e;
        }

        response.setContentLength(input.available());
        response.setHeader("Content-Disposition", "attachment; filename=\"chart.xlsx\"");
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        IOUtils.copy(input, response.getOutputStream());
        return null;
    }

    @RequestMapping(value = "/sequences/stratify/{uuid}")
    public String stratify(@PathVariable("uuid") String uuid, HttpServletResponse response) throws Exception {

        logger.debug("stratify result: {}", uuid);

        SeqBigResultModel result = resultStore.getResult(UUID.fromString(uuid));

        if (result == null) {
            return "entropy/noResult";
        }

        InputStream input;
        try {
            input = Api.createStratifyResult(result.groups, result.intervals, result.seqTotal);
        } catch (Exception e) {
            logger.error("chart creating error", e);
            throw e;
        }

        response.setContentLength(input.available());
        response.setHeader("Content-Disposition", "attachment; filename=\"stratify.xlsx\"");
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        IOUtils.copy(input, response.getOutputStream());
        return null;
    }

}
