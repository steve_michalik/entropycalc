package cz.tomas.entropycalcweb.controller;

import cz.tomas.entropycalclib.Api;
import cz.tomas.entropycalclib.Config;
import cz.tomas.entropycalclib.PnnsCalculation;
import cz.tomas.entropycalclib.Result;
import cz.tomas.entropycalcweb.model.PnnsInputModel;
import cz.tomas.entropycalcweb.utils.ResultStore;
import cz.tomas.entropycalcweb.utils.State;
import cz.tomas.entropycalcweb.utils.TmpFileFactory;
import cz.tomas.entropycalcweb.validator.PnnsInputValidator;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

@Controller
public class PnnsController {

    final Logger logger = LoggerFactory.getLogger(PnnsController.class);

    @Autowired
    private PnnsInputValidator inputValidator;

    @Autowired
    private ResultStore<List<PnnsCalculation>> resultStore;

    @Autowired
    private ExecutorService executor;

    @Autowired
    private TmpFileFactory fileFactory;

    @RequestMapping(value = "/pnns", method = RequestMethod.GET)
    public String dataInput(Model model) {
        model.addAttribute("state", State.PNNS);
        model.addAttribute("command", new PnnsInputModel());
        return "pnns/dataInput";
    }

    @RequestMapping(value = "/pnns", method = RequestMethod.POST)
    public String dataInput(Model model, @ModelAttribute("command") @Valid PnnsInputModel form, BindingResult result) throws IOException, InterruptedException {

        model.addAttribute("state", State.PNNS);
        inputValidator.validate(form, result);

        if (result.hasErrors()) {
            return "pnns/dataInput";
        }

        UUID uuid = UUID.randomUUID();

        try {
            Config config = new Config();
            config.inFile = form.getFile().getInputStream();

            Result apiResult = Api.pnns(config);

            resultStore.addResult(uuid, apiResult.pnns);

            model.addAttribute("uuid", uuid.toString());
            model.addAttribute("result", apiResult);

        } catch (IOException ex) {
            logger.error("file handling error", ex);
            throw ex;
        }

        return "pnns/result";
    }

    @RequestMapping(value = "/pnns/{uuid}")
    public String getResult(@PathVariable("uuid") String uuid, HttpServletResponse response) throws IOException {

        List<PnnsCalculation> result = resultStore.getResult(UUID.fromString(uuid));

        if (result == null)
        {
            return "entropy/noResult";
        }

        InputStream input;
        try {
            input = Api.createPnnsResult(result);
        } catch (Exception e) {
            logger.error("result creating error", e);
            throw e;
        }

        response.setContentLength(input.available());
        response.setHeader("Content-Disposition", "attachment; filename=\"result.xlsx\"");
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        IOUtils.copy(input, response.getOutputStream());
        return null;
    }

    @RequestMapping(value = "/pnns/{uuid}/entropy")
    public String useResultInEntropy(@PathVariable("uuid") String uuids, Model model) throws IOException, InterruptedException {

        UUID uuid = UUID.fromString(uuids);

        List<PnnsCalculation> result = resultStore.getResult(uuid);

        if (result == null)
        {
            return "entropy/noResult";
        }

        try (FileOutputStream output = fileFactory.getNewFile(uuid)) {
            Config config = new Config();
            config.execService = executor;
            config.outFile = output;
            config.useDouble = true;

            Result apiResult = Api.calculateFromPnns(config, result);
            apiResult.useSequence = config.useSequences;

            model.addAttribute("uuid", uuid.toString());
            model.addAttribute("result", apiResult);

        } catch (Exception ex) {
            logger.error("", ex);
            throw ex;
        }

        return "entropy/result";
    }
}
