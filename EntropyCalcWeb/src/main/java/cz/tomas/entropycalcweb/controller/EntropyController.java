package cz.tomas.entropycalcweb.controller;

import cz.tomas.entropycalclib.Api;
import cz.tomas.entropycalclib.Config;
import cz.tomas.entropycalclib.Result;
import cz.tomas.entropycalcweb.model.EntropyInputModel;
import cz.tomas.entropycalcweb.utils.State;
import cz.tomas.entropycalcweb.utils.TmpFileFactory;
import cz.tomas.entropycalcweb.validator.EntropyInputValidator;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

@Controller
public class EntropyController {

    final Logger logger = LoggerFactory.getLogger(EntropyController.class);

    @Autowired
    private EntropyInputValidator inputValidator;

    @Autowired
    private ExecutorService executor;

    @Autowired
    private TmpFileFactory fileFactory;

    @RequestMapping(value = "/entropy", method = RequestMethod.GET)
    public String dataInput(Model model) {
        model.addAttribute("state", State.ENTROPY);
        model.addAttribute("command", new EntropyInputModel());
        return "entropy/dataInput";
    }


    @RequestMapping(value = "/entropy", method = RequestMethod.POST)
    public String dataInputPost(Model model, @ModelAttribute("command") @Valid EntropyInputModel form, BindingResult result) throws IOException, InterruptedException {

        model.addAttribute("state", State.ENTROPY);
        inputValidator.validate(form, result);

        if (result.hasErrors()) {
            return "entropy/dataInput";
        }

        UUID uuid = UUID.randomUUID();

        try (FileOutputStream output = fileFactory.getNewFile(uuid)) {
            Config config = new Config();
            config.execService = executor;
            config.inFile = form.getFile().getInputStream();
            config.outFile = output;
            config.useDouble = true;
            config.useSequences = form.isUseSequences();
//            config.startPosition = form.getStartPosition();
//            config.threshold = form.getThreshold();

            Result apiResult = Api.calculate(config);
            apiResult.useSequence = config.useSequences;

            model.addAttribute("uuid", uuid.toString());
            model.addAttribute("result", apiResult);

        } catch (IOException ex) {
            logger.error("file handling error", ex);
            throw ex;
        }

        return "entropy/result";
    }

    @RequestMapping(value = "/entropy/{uuid}")
    public String getResult(@PathVariable("uuid") String uuid, HttpServletResponse response) throws IOException {
        File file = fileFactory.getFile(UUID.fromString(uuid));

        if (!file.isFile()) {
            return "entropy/noResult";
        }

        response.setContentLength((int)file.length());
        response.setHeader("Content-Disposition", "attachment; filename=\"result.xls\"");
        response.setContentType("application/vnd.ms-excel");

        IOUtils.copy(new FileInputStream(file), response.getOutputStream());
        return null;
    }
}
