package cz.tomas.entropycalcweb.controller;

import com.google.common.collect.Range;
import cz.tomas.entropycalclib.Api;
import cz.tomas.entropycalclib.Config;
import cz.tomas.entropycalclib.Result;
import cz.tomas.entropycalcweb.exception.BadInputException;
import cz.tomas.entropycalclib.Interval;
import cz.tomas.entropycalcweb.model.SeqBigResultModel;
import cz.tomas.entropycalcweb.model.SeqInputModel;
import cz.tomas.entropycalcweb.model.SeqResultModel;
import cz.tomas.entropycalcweb.utils.ResultStore;
import cz.tomas.entropycalcweb.validator.SeqInputValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

@RestController
public class ApiController {

	final Logger logger = LoggerFactory.getLogger(ApiController.class);

	@Autowired
	private SeqInputValidator inputValidator;

	@Autowired
	private ResultStore<SeqBigResultModel> resultStore;


	@RequestMapping(value = "/sequences/results/{uuid}", method = RequestMethod.GET)
	public SeqBigResultModel seqResult(@PathVariable("uuid") String uuid) {

		logger.debug("getting result: {}", uuid);
		SeqBigResultModel result = resultStore.getResult(UUID.fromString(uuid));

		if (result == null)
		{
			throw new BadInputException();
		}

		return result;
	}

	@RequestMapping(value = "/sequences/results/{uuid}", method = RequestMethod.HEAD)
	public ResponseEntity touchSeqResult(@PathVariable("uuid") String uuid) {

		logger.debug("touching result: {}", uuid);
		SeqBigResultModel result = resultStore.getResult(UUID.fromString(uuid));
		return new ResponseEntity(result != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/sequences/results/{uuid}", method = RequestMethod.DELETE)
	public ResponseEntity deleteSeqResult(@PathVariable("uuid") String uuid) {

		logger.debug("deleting result: {}", uuid);
		boolean result = resultStore.markForDelete(UUID.fromString(uuid));
		return new ResponseEntity(result ? HttpStatus.OK : HttpStatus.NOT_FOUND);
	}

	private static Collection<Interval> mergeIntervals(Interval[] intervals) {

		if (intervals == null) {
			return null;
		}

		Collection<Interval> mergedIntervals = new ArrayList<>(intervals.length);

		Arrays.sort(intervals, Comparator.comparingInt(Interval::getStart));

		for (int i = 0; i < intervals.length; ) {
			Range<Integer> range = Range.closed(intervals[i].getStart(), intervals[i].getEnd());

			int j = i + 1;
			for (; j < intervals.length; j++) {
				Range<Integer> other = Range.closed(intervals[j].getStart(), intervals[j].getEnd());

				if (!range.isConnected(other)) {
					break;
				}

				range = range.span(other);
			}

			mergedIntervals.add(new Interval(range.lowerEndpoint(), range.upperEndpoint()));
			i = j;
		}

		return mergedIntervals;
	}

	@RequestMapping(value = "/sequences", method = RequestMethod.POST)
	public SeqResultModel seqInputPost(Model model, @Valid SeqInputModel form, BindingResult result) throws IOException, InterruptedException {

		inputValidator.validate(form, result);

		if (result.hasErrors()) {
			throw new BadInputException();
		}

		Collection<Interval> mergedIntervals = mergeIntervals(form.getIntervals());

		UUID uuid = UUID.randomUUID();
		Config config;
		Result apiResult;
		SeqBigResultModel seqResult;

		try {
			config = new Config();
			config.inFile = form.getFile().getInputStream();
			config.useDouble = true;
			config.intervals = mergedIntervals;
			config.wholeSequence = form.isUseAll();
			config.threshold = 100;

			apiResult = Api.group(config);
			apiResult.useSequence = config.useSequences;

			model.addAttribute("uuid", uuid.toString());
			model.addAttribute("result", apiResult);

		} catch (IOException ex) {
			logger.error("file handling error", ex);
			throw new BadInputException();
		} catch (IllegalArgumentException ex) {
			throw new BadInputException();
		}

		seqResult = new SeqBigResultModel();
		seqResult.time = apiResult.time;
		seqResult.seqLength = apiResult.positions;
		seqResult.seqTotal = apiResult.sequencesTotal;
		seqResult.groups = apiResult.groups;
		seqResult.intervals = apiResult.intervals;

		resultStore.addResult(uuid, seqResult);

		return new SeqResultModel(uuid.toString());
	}
}
