/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalcweb.validator;

import cz.tomas.entropycalcweb.model.EntropyInputModel;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Tomas
 */
@Component
public class EntropyInputValidator implements Validator {

	@Override
	public boolean supports(Class<?> type) {
		return EntropyInputModel.class.isAssignableFrom(type);
	}

	@Override
	public void validate(Object o, Errors errors) {
		EntropyInputModel model = (EntropyInputModel)o;

		if (model.getFile().isEmpty()) {
			errors.rejectValue("file", "NotNull");
		}
	}
	
}
