/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalcweb.validator;

import cz.tomas.entropycalclib.Interval;
import cz.tomas.entropycalcweb.model.SeqInputModel;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Tomas
 */
@Component
public class SeqInputValidator implements Validator {

	@Override
	public boolean supports(Class<?> type) {
		return SeqInputModel.class.isAssignableFrom(type);
	}

	@Override
	public void validate(Object o, Errors errors) {
		SeqInputModel model = (SeqInputModel)o;

		if (model.getFile().isEmpty()) {
			errors.rejectValue("file", "NotNull");
		}

		if (!model.isUseAll()) {
			if (model.getIntervals() == null || model.getIntervals().length == 0) {
				errors.rejectValue("end", "NotNull");
			} else {
				for (Interval i : model.getIntervals()) {
					if (i.getStart() >= i.getEnd()) {
						errors.rejectValue("end", "SeqInput.end.small");
					}
				}
			}
		}
	}
}
