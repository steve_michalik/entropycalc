package cz.tomas.entropycalcweb.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Tomas on 13/12/2015.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadInputException extends RuntimeException {

    public BadInputException() {
        this("bad input.");
    }

    public BadInputException(String message) {
        super(message);
    }

}
