(function () {
    'use strict';

    angular.module('app').controller('sequencesCtrl', Ctrl);

    Ctrl.$inject = ['$scope', '$filter', 'FileSaver', 'Blob', '$window', 'Results', 'Const', '$interval'];
    function Ctrl($scope, $filter, FileSaver, Blob, $window, Results, Const, $interval) {

        $scope.uuid = $window.resultUuid;
        $scope.export = [];
        $scope.loading = true;
        $scope.error = false;

        var params = {uuid: $scope.uuid};
        $scope.result = Results.get(params, function (result) {
            result.init();
            $scope.loading = false;

            $scope.chart.data = [{
                key: 'Frequency',
                values: result.groups
            }];

            $scope.chart.api.refresh();

            $interval(function() {
                result.$touch(params);
            }, 60*1000);

            $window.addEventListener('beforeunload', function(event) {
                result.$delete(params);
            });

        }, function (err) {
            $scope.error = true;
            $scope.loading = false;
        });

        $scope.compress = function () {

            var result = '';
            var filt = $filter('number');

            angular.forEach($scope.result.groups, function (group) {
                result += '>group ' + group.name + ' frequency ' + filt(group.probability, 3) + '%\r\n' + group.sequences[0].data + '\r\n';
            });

            var blob = new Blob([result], {type: 'text/plain'});
            FileSaver.saveAs(blob, 'compressed.txt');
        };

        $scope.showGroup = function (group) {
            if ($scope.group) {
                $scope.group.selected = false;
            }
            $scope.group = group;
            group.selected = true;
        };

        $scope.addExport = function (seq) {
            //$scope.export[seq.number] = seq;
            if (!seq.exported) {
                $scope.export.push(seq);
                seq.exported = true;
            }
        };

        $scope.removeExport = function (seq) {
            //delete $scope.export[seq.number];
            if (seq.exported) {
                var index = $scope.export.indexOf(seq);
                if (index >= 0) {
                    $scope.export.splice(index, 1);
                }
                seq.exported = false;
            }
        };

        $scope.toggleExport = function (seq) {
            seq.exported ? $scope.removeExport(seq) : $scope.addExport(seq);
        };

        $scope.addRemoveAll = function (group, add) {
            if (!group) {
                return;
            }

            angular.forEach(group.sequences, function (seq) {
                add ? $scope.addExport(seq) : $scope.removeExport(seq);
            });
        };

        function exportHelper(seqs) {
            var result = '';

            angular.forEach(seqs, function (seq) {
                result += seq.meta + '\r\n' + seq.data + '\r\n';
            });

            var blob = new Blob([result], {type: 'text/plain'});
            FileSaver.saveAs(blob, 'export.txt');
        }

        $scope.exportRemoveAll = function () {
            angular.forEach($scope.export, function (seq) {
                seq.exported = false;
            });

            $scope.export = [];
        };

        $scope.exportExport = function () {
            exportHelper($scope.export);
        };

        $scope.exportInformative = function () {
            exportHelper($scope.result.informativeSeqs);
        };

        $scope.canAddRemoveAll = function (group, add) {
            if (!group) {
                return false;
            }

            return group.sequences.some(function (s) {
                return add ? !s.exported : s.exported;
            });
        };

        $scope.canExport = function () {
            return $scope.export.length > 0;
        };


        $scope.chart = {
            options: {
                chart: {
                    type: 'historicalBarChart',
                    height: 300,
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 65,
                        left: 50
                    },
                    x: function (d) {
                        return d.number;
                    },
                    y: function (d) {
                        return d.probability;
                    },
                    showValues: true,
                    xAxis: {
                        axisLabel: 'Variant Group'
                    },
                    yAxis: {
                        axisLabel: 'Frequency %',
                        axisLabelDistance: -10,
                        tickFormat: function(d) {
                            return d3.format(',.1f')(d);
                        }
                    },
                    tooltip: {
                        //valueFormatter: function(d) {
                        //    return d3.format(',.1f')(d) + '%';
                        //}
                        contentGenerator: function (o) {
                            var d = o.data;
                            return d.name + ' <strong>' + d3.format(',.1f')(d.probability) + '%</strong>';
                        }
                    },
                    zoom: {
                        enabled: true,
                        scaleExtent: [1, 10],
                        useFixedDomain: true,
                        useNiceScale: false,
                        verticalOff: true,
                        unzoomEventType: 'dblclick.zoom'
                    }
                }
            },

            config: {
                deepWatchDataDepth: 0
            },

            data: []
        };
    }
})();
