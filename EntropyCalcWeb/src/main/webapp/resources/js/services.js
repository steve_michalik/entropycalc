(function () {
    'use strict';

    angular.module('app')
        .factory('Results', Results);

    Results.$inject = ['$resource', 'Const'];
    function Results ($resource, Const) {

        var url = Const.baseUrl + 'sequences/results/:uuid';
        var results = $resource(url, {},
            {
                touch: {method: 'HEAD', url: url}
            });

        angular.extend(results.prototype, {

            init: function () {
                var result = this;

                result.groupsLength = result.groups.reduce(function (total, gr) {
                   return gr.type == 'NORMAL' ? total + 1 : total;
                }, 0);

                result.groups.sort(function (a, b) {
                   return a.priority*1E10 - b.priority*1E10 + b.sequences.length - a.sequences.length;
                });

                result.informativeSeqs = [];

                angular.forEach(result.groups, function (g, i) {
                    g.number = g.name = i+1;
                    g.freq = g.sequences.length;
                    g.probability = g.freq / result.seqTotal * 100;

                    if (g.priority !== 0) {
                        g.name = g.type.toLowerCase();
                        g.color = 'red';
                    }

                    angular.forEach(g.sequences, function (s) {
                        s.group = g;
                    });


                    if (g.type == 'NORMAL') {
                        result.informativeSeqs = result.informativeSeqs.concat(g.sequences);
                    }
                });
            }
        });
        return results;
    }
})();