(function () {
    'use strict';

    angular.module('app').controller('seqInputCtrl', Ctrl);

    Ctrl.$inject = ['$scope', '$window', 'Const', '$http'];
    function Ctrl($scope, $window, Const, $http) {

        $scope.data = {
            //threshold: 100,
            useAll: true,
            start: 1,
            end: 1000,
            intervals : [{start: 1, end: 1000}]
        };

        $scope.processing = false;

        $scope.addInterval = function () {
            $scope.data.intervals.push({start: 1, end: 1000});
        };

        $scope.removeInterval = function (index) {
            $scope.data.intervals.splice(index, 1);
        };

        $scope.save = function () {

            var data = $scope.data;

            if (!data.end) {
                data.end = 1000;
            }

            $scope.processing = true;

            $http.post(Const.baseUrl + 'sequences',
                new FormData(document.getElementById('form')),
                { headers: { 'Content-Type': undefined } }
            ).then(
                function (resp) {
                    $window.location.href = Const.baseUrl + 'sequences/' + resp.data.uuid;
                },
                function () {
                    $scope.processing = false;
                    $scope.err = true;
                });
        }
    }
})();
