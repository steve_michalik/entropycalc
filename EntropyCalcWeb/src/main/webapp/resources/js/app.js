(function () {
    'use strict';

    angular.module('app', [
        'ngResource',
        'ngFileUpload',
        'ngFileSaver',
        'smart-table',
        'nvd3'
    ])
        .config(Cfg)
        .factory('Const', Const);


    Cfg.$inject = ['$locationProvider', '$compileProvider'];
    function Cfg($locationProvider, $compileProvider) {
        $compileProvider.debugInfoEnabled(false);
    }

    Const.$inject = ['$window'];
    function Const($window) {
        return {
            baseUrl: $window.baseUrl
        }
    }

})();
