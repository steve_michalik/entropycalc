<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="jwr" uri="http://jawr.net/tags" %>
<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Alignment Explorer<sitemesh:write property='title'/></title>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<jwr:style src="/bundles/app.css"/>
		<link rel="shortcut icon" type="image/png" href="<spring:url value="/resources/favicon.png"/>">

	<sitemesh:write property='head'/>
</head>

<body>
	<!-- Fixed navbar -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<spring:url value="/"/>">Alignment Explorer</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<%--<li <c:if test="${state == 'HOME'}">class="active"</c:if>><a href="<spring:url value="/"/>">Home</a></li>--%>
					<li <c:if test="${state == 'PNNS'}">class="active"</c:if>><a href="<spring:url value="/pnns"/>">PNNS Calculator</a></li>
                    <li <c:if test="${state == 'ENTROPY'}">class="active"</c:if>><a href="<spring:url value="/entropy"/>">Entropy Calculator</a></li>
                    <li <c:if test="${state == 'SEQUENCE'}">class="active"</c:if>><a href="<spring:url value="/sequences"/>">Sequence Tracer</a></li>
						<%--<li><a href="<spring:url value="/"/>">Advanced Applications</a></li>--%>
					<li <c:if test="${state == 'LINKS'}">class="active"</c:if>><a href="<spring:url value="/links"/>">Useful Links</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">
		<sitemesh:write property='body'/>
	</div>


	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
                    <div class="text-muted">
                        <noscript>
                            <img src="http://c.statcounter.com/10616649/0/359c6184/0/"/>
                        </noscript>
                        <script type="text/javascript">
                            var sc_project = 10616649;
                            var sc_invisible = 0;
                            var sc_security = "359c6184";
                            var scJsHost = (("https:" == document.location.protocol) ?
                                    "https://secure." : "http://www.");
                            document.write("<sc" + "ript type='text/javascript' src='" +
                                    scJsHost +
                                    "statcounter.com/counter/counter.js'></" + "script>");
                        </script>
                    </div>
                </div>
				<div class="col-md-6">
					<div class="text-muted text-right">
						<small><em>v1.4.3</em></small>
                    </div>
				</div>
			</div>
		</div>
	</footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>