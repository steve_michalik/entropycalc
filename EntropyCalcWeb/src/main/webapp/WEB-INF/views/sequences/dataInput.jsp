<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
    <title> | Sequence Tracer | Input Data</title>
</head>
<body>
<div class="page-header">
    <h2>New calculation</h2>
    <h5>The Sequence Tracer assesses the number of sequence variants and sorts the sequences within a multiple nucleic acid sequence alignment.</h5>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="alert alert-info">
            <strong>The input file can be of the following types:</strong>
            <ul>
                <li>
                    Validated sequence alignment (txt, fasta).
                    <a href="<spring:url value="/resources/sequences.txt"/>" class="btn btn-default btn-xs">
                        <span class="glyphicon glyphicon-save-file"></span> Example
                    </a>
                </li>
            </ul>
        </div>

        <div ng-show="err" class="alert alert-danger">
            There was error processing the input.
        </div>

        <form id="form" name="form" ng-submit="save()" novalidate="" enctype="multipart/form-data">

            <div class="form-group" ng-class="{'has-error': form.file.$touched && form.file.$invalid}">
                <label class="control-label">Input File</label>
                <input type="file" name="file" required=""
                       ngf-select="" ng-model="data.file" class="form-control"/>
            </div>

            <%--
            <div class="form-group">
                <label class="control-label">Threshold</label>
                <div class="input-group">
                    <input type="number" name="threshold" required=""
                           ng-model="data.threshold" min="1" max="100" class="form-control"/>
                    <span class="input-group-addon">%</span>
                </div>
                <span class="help-block">Two sequences are declared equal when they are same in this number percent cases.</span>
            </div>
            --%>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title">Region of Interest</span>
                </div>
                <div class="panel-body">

                    <div class="checkbox checkbox-primary">
                        <input type="checkbox" id="useAll" name="useAll" ng-model="data.useAll" class="styled"/>
                        <label for="useAll">Entire Alignment</label>
                    </div>

                    <div ng-if="!data.useAll">
                        <table class="table">
                            <tr>
                                <th>Start</th>
                                <th>End</th>
                                <th></th>
                            </tr>
                            <tr ng-repeat="interval in data.intervals">
                                <td>
                                    <input type="number" name="intervals[{{$index}}].start" required="" ng-model="interval.start" min="1" class="form-control"/>
                                </td>
                                <td>
                                    <input type="number" name="intervals[{{$index}}].end" required="" ng-model="interval.end" min="{{interval.start+1}}" class="form-control"/>
                                </td>
                                <td style="width: 60px;">
                                    <button type="button" ng-if="!$first" ng-click="removeInterval($index)" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <button type="button" ng-click="addInterval()" class="btn btn-success">
                                        <span class="glyphicon glyphicon-plus"></span> Add Interval
                                    </button>
                                </td>
                            </tr>
                        </table>
                        <%--<div class="form-group" ng-class="{'has-error': !data.useAll && form.start.$touched && form.start.$invalid}">--%>
                            <%--<label class="control-label">Start Position</label>--%>
                            <%--<input type="number" name="start" required=""--%>
                                   <%--ng-model="data.start" min="1" class="form-control"/>--%>
                            <%--<span class="help-block">Index of the first position, which will be processed. Positions before this one are skipped.</span>--%>
                        <%--</div>--%>

                        <%--<div class="form-group" ng-class="{'has-error': !data.useAll && form.end.$touched && form.end.$invalid}">--%>
                            <%--<label class="control-label">End Position</label>--%>
                            <%--<input type="number" name="end" required=""--%>
                                   <%--ng-model="data.end" min="{{data.start+1}}" class="form-control"/>--%>
                            <%--<span class="help-block">Index of the last position, which will be processed. Positions after this one are skipped.</span>--%>
                        <%--</div>--%>
                    </div>

                </div>
            </div>

            <button type="submit" ng-disabled="form.$pristine || form.$invalid" class="btn btn-primary">
                <span ng-show="processing" class="fa fa-spin fa-spinner"></span> Submit
            </button>
        </form>
    </div>
</div>
</body>
</html>
