<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
        <title> | Sequence Tracer | Trace the Variants</title>
    </head>
    <body>
		<div class="page-header">
			<h2>Trace the Variants</h2>
		</div>

        <div ng-show="loading" class="row">
            <div class="col-md-12">
                <h2><span class="fa fa-spin fa-spinner"></span> Loading result ...</h2>
            </div>
        </div>
        <div ng-show="error" class="row">
            <div class="col-md-12">
                <div class="alert alert-danger">The requested result was not found.</div>
            </div>
        </div>

        <div ng-show="!loading && !error">
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Summary</h3>
					</div>

					<div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-hover table-condensed" style="margin-bottom: 25px">
                                    <tr>
                                        <td>Total number of sequences</td>
                                        <td>{{result.seqTotal}}</td>
                                    </tr>
                                    <tr>
                                        <td>Number of informative sequences</td>
                                        <td>{{result.informativeSeqs.length}}</td>
                                    </tr>
                                    <tr>
                                        <td>Sequence length</td>
                                        <td>{{result.seqLength}}</td>
                                    </tr>
                                    <tr>
                                        <td>Number of groups</td>
                                        <td>{{result.groupsLength}}</td>
                                    </tr>
                                    <tr>
                                        <td>Calculation time</td>
                                        <td>{{result.time}} ms</td>
                                    </tr>
                                    <tr>
                                        <td>Selection from-to</td>
                                        <td>
                                            <p ng-repeat="interval in result.intervals">{{interval.start}}-{{interval.end}}</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button ng-click="exportInformative()" class="btn btn-success"
                                        title="Exports all informative sequences, ie. all sequence groups except outgroups 1, 2 and excluded."
                                        style="width: 80%; margin: 3px 0">
                                    <span class="glyphicon glyphicon-download-alt"></span> Export Informatives</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button ng-click="compress()" class="btn btn-success"
                                        title="Picks one representative sequence from each group and prepares a weighted alignment."
                                        style="width: 80%; margin: 3px 0">
                                    <span class="glyphicon glyphicon-compressed"></span> Compress</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a ng-href="<spring:url value="/sequences/stratify/{{uuid}}"/>" class="btn btn-success"
                                   title="Picks one representative sequence from each group and prepares a graphical output."
                                   style="width: 80%; margin: 3px 0">
                                    <span class="glyphicon glyphicon-cog"></span> Stratify</a>
                            </div>
                        </div>
                    </div>
                </div>
			</div>

            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left" style="margin-right: 5px">Histogram </h3>
                        <span class="glyphicon glyphicon-question-sign"
                              title="Swipe left/right to focus specific area. Double-click to reset zoom."></span>
                    </div>

                    <div class="panel-body">
                        <nvd3 options="chart.options" data="chart.data" config="chart.config" api="chart.api"></nvd3>
                    </div>
                    <div class="panel-footer">
                        <a ng-href="<spring:url value="/sequences/chart/{{uuid}}"/>" class="btn btn-success">
                            <span class="glyphicon glyphicon-download-alt"></span> Export Chart</a>
                    </div>
                </div>
            </div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Groups</h3>
					</div>

					<div class="panel-body" style="overflow: auto; max-height: 800px">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th>Group Number</th>
                                <th>Variant Count</th>
                                <th>Frequency</th>
                            </tr>
                            <tr ng-repeat="gr in result.groups" ng-click="showGroup(gr)" ng-class="{warning: gr.selected}"
                                title="{{{EXCLUDED: 'Submissions containing no data.',
                                        OUTGROUP1: 'Sequences containing ambiguous positions.',
                                        OUTGROUP2: 'Incomplete sequences.',
                                        NORMAL: ''}[gr.type]}}">
                                <td>{{gr.name}}</td>
                                <td>{{gr.freq}}</td>
                                <td>{{gr.probability | number: 3}}%</td>
                            </tr>
                        </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-9">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Group Details</h3>
                                </div>

                                <div class="panel-body" style="overflow: auto; height: 300px">
                                    <table class="table table-bordered table-hover">
                                        <tr>
                                            <th></th>
                                            <th>Group Number</th>
                                            <th>Sequence Number</th>
                                            <th>Sequence</th>
                                        </tr>
                                        <tr ng-repeat="seq in group.sequences" ng-class="{warning: seq.exported}">
                                            <td>
                                                <button ng-click="toggleExport(seq)"
                                                        class="btn center-block" ng-class="{'btn-success': !seq.exported, 'btn-danger': seq.exported}">
                                                    <div ng-show="!seq.exported">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </div>
                                                    <div ng-show="seq.exported">
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    </div>
                                                </button>
                                            </td>
                                            <td>{{seq.group.name}}</td>
                                            <td>{{seq.number}}</td>
                                            <td>{{seq.meta}}</td>
                                        </tr>
                                    </table>
                                </div>
                            <div class="panel-footer">
                                <button ng-click="addRemoveAll(group, true)" ng-disabled="!canAddRemoveAll(group, true)" class="btn btn-success">
                                    <span class="glyphicon glyphicon-plus"></span> Add all to Notes</button>
                                <button ng-click="addRemoveAll(group, false)" ng-disabled="!canAddRemoveAll(group, false)" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-minus"></span> Remove All</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>NOTES</strong></h3>
                            </div>

                            <div class="panel-body" style="overflow: auto; height: 330px">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th></th>
                                        <th>Group Number</th>
                                        <th>Sequence Number</th>
                                        <th>Sequence</th>
                                    </tr>
                                    <tr ng-repeat="seq in export">
                                        <td>
                                            <button ng-click="removeExport(seq)" class="btn btn-danger center-block">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </td>
                                        <td>{{seq.group.name}}</td>
                                        <td>{{seq.number}}</td>
                                        <td>{{seq.meta}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <button ng-click="exportRemoveAll()" ng-disabled="!canExport()" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-minus"></span> Remove All</button>
                                <button ng-click="exportExport()" ng-disabled="!canExport()" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-download-alt"></span> Export</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
		var resultUuid = '${uuid}';
	</script>
    </body>
</html>
