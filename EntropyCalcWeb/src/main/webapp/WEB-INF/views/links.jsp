<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
        <title> | Useful Links</title>
    </head>
    <body>
		<div class="page-header">
			<h2>Useful Links</h2>
		</div>

		<ul>
			<li>
				<a href="http://mafft.cbrc.jp/alignment/software/" target="_blank">MAFFT</a> - multiple alignment program for amino acid or nucleotide sequences
			</li>
			<li>
				<a href="http://www.mbio.ncsu.edu/bioedit/bioedit.html" target="_blank">BioEdit</a> - a biological sequence alignment editor
			</li>
			<li>
				<a href="http://www.ormbunkar.se/aliview/" target="_blank">AliView</a> - fast alignment viewer and editor, works swiftly with large alignments
			</li>
			<li>
				<a href="http://users-birc.au.dk/biopv/php/fabox/" target="_blank">FaBox</a> - an online fasta sequence toolbox
			</li>
			<li>
				<a href="http://eu.idtdna.com/calc/analyzer/" target="_blank">Oligo Analyzer</a>
			</li>
		</ul>

    </body>
</html>
