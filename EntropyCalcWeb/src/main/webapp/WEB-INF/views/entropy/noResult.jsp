<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
	<head>
		<title> | Entropy Calculator | Result</title>
	</head>
	<body>
	<div class="page-header">
		<h2>No result found</h2>
	</div>

	<p>The result was not found, probably it was too old and has been already deleted.</p>
	<p>Please use the calculator to create new result.</p>
	</body>
</html>
