<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
        <title> | Entropy Calculator | Input Data</title>
    </head>
    <body>
		<div class="page-header">
			<h2>New calculation</h2>
			<h5>The Entropy Calculator quantifies the amount of variability through each column of a multiple nucleic acid sequence alignment.</h5>
		</div>

		<div class="row">
			<div class="col-md-12">

				<div class="alert alert-info">
					<strong>The input file can be of the following types:</strong>
					<ul>
						<li>
							Positional Nucleotide Numerical Summary (PNNS, BioEdit) data.
							<a href="<spring:url value="/resources/elisa.txt"/>" class="btn btn-default btn-xs">
								<span class="glyphicon glyphicon-save-file"></span> Example
							</a>
						</li>
						<%--
						<li>
							File containing sequences. For this type of input please check the 'Use Sequences' checkbox.
							<a href="<spring:url value="/resources/sequences.txt"/>" class="btn btn-default btn-xs">
								<span class="glyphicon glyphicon-save-file"></span> Example
							</a>
						</li>
						--%>
					</ul>
				</div>

				<spring:url var="action" value="/entropy"/>
				<form:form method="POST" action="${action}" enctype="multipart/form-data">
					<c:set var="errors" value="${requestScope['org.springframework.validation.BindingResult.command']}"/>

					<div class="form-group ${errors.hasFieldErrors('file') ? 'has-error' : ''}">
						<form:label cssClass="control-label" path="file">Input File</form:label>
						<form:input cssClass="form-control" path="file" type="file"/>
						<form:errors path="file" cssClass="help-block" />
					</div>


					<%--
					<div class="panel panel-default">
						<div class="panel-heading">Sequences input</div>
						<div class="panel-body">

					<div class="form-group">
						<form:label cssClass="control-label" path="useSequences">Use Sequences</form:label>
						<form:checkbox path="useSequences"/>
					</div>

					<div class="form-group ${errors.hasFieldErrors('threshold') ? 'has-error' : ''}">
						<form:label cssClass="control-label" path="threshold">Threshold</form:label>
						<div class="input-group">
							<form:input cssClass="form-control" path="threshold" type="number"/>
							<span class="input-group-addon">%</span>
						</div>
						<form:errors path="threshold" cssClass="help-block"/>
						<span class="help-block">Two sequences are declared equal when they are same in this number percent cases.</span>
					</div>

					<div class="form-group ${errors.hasFieldErrors('startPosition') ? 'has-error' : ''}">
						<form:label cssClass="control-label" path="startPosition">Start Position</form:label>
						<form:input cssClass="form-control" path="startPosition" type="number"/>
						<form:errors path="startPosition" cssClass="help-block"/>
						<span class="help-block">Index of the first position, which will be processed. Positions before this one are skipped.</span>
					</div>
				</div>
			</div>
					--%>

					<input class="btn btn-primary" type="submit" value="Submit"/>
				</form:form>
			</div>
		</div>
		<div class="row" style="margin-top: 20px">
			<div class="col-md-12">
				<strong>Reference: Nagy A, Jiřinec T, Černíková L, Jiřincová H, Havlíčková M.</strong>
				Large-scale nucleotide sequence alignment and sequence variability assessment to identify the evolutionarily highly conserved regions for universal screening PCR assay design: an example of influenza A virus.<br/>
				Methods Mol Biol. 2015;1275: 57-72. doi: 10.1007/978-1-4939-2365-6_4.
				<p>
					<a style="margin-top: 10px" class="btn btn-default" href="<spring:url value="/resources/poster.pdf"/>" role="button">Quick Guide</a>
				</p>
			</div>
		</div>
    </body>
</html>
