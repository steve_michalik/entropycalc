<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
        <title> | Entropy Calculator | Result</title>
    </head>
    <body>
		<div class="page-header">
			<h2>Result</h2>
		</div>

		<p>Found ${result.positions} positions.</p>
		<p>Calculation finished in ${result.time} ms.</p>

		<c:if test="${result.useSequence}">
			<p>Found ${result.sequences} (${result.sequencesPercent}%) unique sequences after the filtering.</p>
		</c:if>

		<a class="btn btn-primary" href="<spring:url value="/entropy/${uuid}"/>">Download Result</a>
		<a class="btn btn-default" href="<spring:url value="/entropy"/>">New Calculation</a>
    </body>
</html>
