<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
        <title> | PNNS Calculator | Result</title>
    </head>
    <body>
		<div class="page-header">
			<h2>Result</h2>
		</div>

		<p>Found ${result.positions} positions in ${result.sequences} sequences.</p>
		<p>Calculation finished in ${result.time} ms.</p>

		<a class="btn btn-primary" href="<spring:url value="/pnns/${uuid}"/>">Download Result</a>
		<a class="btn btn-default" href="<spring:url value="/pnns/${uuid}/entropy"/>">Proceed directly to Entropy Calculator</a>
		<a class="btn btn-default" href="<spring:url value="/pnns"/>">New Calculation</a>
    </body>
</html>
