<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
        <title> | PNNS Calculator | Input Data</title>
    </head>
    <body>
		<div class="page-header">
			<h2>New calculation</h2>
			<h5>The PNNS (positional nucleotide numerical summary) calculator summarises the characters in each
				column of the multiple sequence alignment.</h5>
		</div>

		<div class="row">
			<div class="col-md-12">

				<div class="alert alert-info">
					<strong>The input file can be of the following types:</strong>
					<ul>
						<li>
							Validated sequence alignment (txt, fasta).
							<a href="<spring:url value="/resources/sequences.txt"/>" class="btn btn-default btn-xs">
								<span class="glyphicon glyphicon-save-file"></span> Example
							</a>
						</li>
					</ul>
				</div>

				<spring:url var="action" value="/pnns"/>
				<form:form method="POST" action="${action}" enctype="multipart/form-data">
					<c:set var="errors" value="${requestScope['org.springframework.validation.BindingResult.command']}"/>

					<div class="form-group ${errors.hasFieldErrors('file') ? 'has-error' : ''}">
						<form:label cssClass="control-label" path="file">Input File</form:label>
						<form:input cssClass="form-control" path="file" type="file"/>
						<form:errors path="file" cssClass="help-block" />
					</div>

					<input class="btn btn-primary" type="submit" value="Submit"/>
				</form:form>
			</div>
		</div>
    </body>
</html>
