<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
    <head>
    </head>
    <body>
		<div class="jumbotron">
			<h1>Welcome in Alignment Explorer</h1>
			<p>
				<a class="btn btn-lg btn-primary" href="<spring:url value="/pnns"/>" role="button">PNNS Calculator &raquo;</a>
				<a class="btn btn-lg btn-primary" href="<spring:url value="/entropy"/>" role="button">Entropy Calculator &raquo;</a>
				<a class="btn btn-lg btn-primary" href="<spring:url value="/sequences"/>" role="button">Sequence Tracer &raquo;</a>
			</p>
			<p style="margin-top: 40px">This web service is provided by:<br/>
					<a href="http://www.szu.cz/">
						<em>The National Institute of Public Health, Czech Republic.</em>
						<img src="<spring:url value="/resources/szu_logo.png"/>" width="50" height="50"/>
					</a>
			</p>
		</div>
    </body>
</html>
