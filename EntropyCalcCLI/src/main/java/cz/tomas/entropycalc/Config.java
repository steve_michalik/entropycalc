/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalc;

/**
 *
 * @author Tomas
 */
public class Config {
	
	public String inFile;
	public String outFile;

	public int precision;
	public boolean useDouble;

	public int threadCount;

	public boolean useSequences;
	public int threshold;
	public int startPosition;
}
