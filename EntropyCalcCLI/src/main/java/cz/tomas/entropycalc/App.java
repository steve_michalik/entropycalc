package cz.tomas.entropycalc;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.UnflaggedOption;
import cz.tomas.entropycalclib.Api;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.Executors;

public class App 
{
	static final String INFILE = "infile";
	static final String OUTFILE = "outfile";
	static final String PRECISION = "precision";
	static final String THREAD_COUNT = "thread_count";
	static final String USE_DOUBLE = "use_double";
	static final String USE_SEQ = "use_seq";
	static final String THRESHOLD = "threshold";
	static final String START_POS = "start_pos";


	private static boolean parseCmdLine(String[] args, Config config) throws JSAPException {

		JSAP jsap = new JSAP();
		Parameter opt = new UnflaggedOption(INFILE).setRequired(true)
								.setHelp("Input file name.");
		jsap.registerParameter(opt);

		opt = new UnflaggedOption(OUTFILE).setRequired(true)
								.setHelp("Output file name.");
		jsap.registerParameter(opt);

		opt = new FlaggedOption(PRECISION).setStringParser(JSAP.INTEGER_PARSER)
											.setDefault("100").setShortFlag('p')
											.setLongFlag("precision")
											.setHelp("Number of digits of precision the calculation is using.");
		jsap.registerParameter(opt);

		opt = new FlaggedOption(THREAD_COUNT).setStringParser(JSAP.INTEGER_PARSER)
											.setDefault("2").setShortFlag('t')
											.setLongFlag("threads")
											.setHelp("Number of threads computing the calculation.");
		jsap.registerParameter(opt);

		opt = new Switch(USE_DOUBLE).setShortFlag('d').setLongFlag("double")
				.setHelp("Use the double-precision binary floating-point numbers. (Fast but could be imprecise.)");
		jsap.registerParameter(opt);



		opt = new FlaggedOption(THRESHOLD).setStringParser(JSAP.INTEGER_PARSER)
											.setDefault("95").setShortFlag('l')
											.setLongFlag("limit")
											.setHelp("Two sequences are declared equal when they are same in this number percent cases.");
		jsap.registerParameter(opt);

		opt = new FlaggedOption(START_POS).setStringParser(JSAP.INTEGER_PARSER)
											.setDefault("1").setShortFlag('s')
											.setLongFlag("start")
											.setHelp("Index of the first position, which will be processed. Positions before this one are skipped.");
		jsap.registerParameter(opt);

		opt = new Switch(USE_SEQ).setShortFlag('q').setLongFlag("seq")
				.setHelp("Use this flag when the sequences input file is used.");
		jsap.registerParameter(opt);


		JSAPResult jsapConfig = jsap.parse(args);

		if (!jsapConfig.success()) {
			
			System.err.println();

			for (Iterator it = jsapConfig.getErrorMessageIterator(); it.hasNext();) {
				System.err.println("Error: " + it.next());
			}

			System.err.println();
			System.err.println("Usage: entropycalc " + jsap.getUsage());
			System.err.println();
			System.err.println(jsap.getHelp());

			return false;
		}
		
		config.inFile = jsapConfig.getString(INFILE);
		config.outFile = jsapConfig.getString(OUTFILE);
		config.precision = jsapConfig.getInt(PRECISION);
		config.threadCount = jsapConfig.getInt(THREAD_COUNT);
		config.useDouble = jsapConfig.getBoolean(USE_DOUBLE);

		config.useSequences = jsapConfig.getBoolean(USE_SEQ);
		config.threshold = jsapConfig.getInt(THRESHOLD);
		config.startPosition = jsapConfig.getInt(START_POS);


		return true;
	}

	
    public static void main( String[] args ) throws IOException, JSAPException, InterruptedException
    {
		Config config = new Config();

		if (!parseCmdLine(args, config))
			System.exit(1);

		cz.tomas.entropycalclib.Config apiConfig = new cz.tomas.entropycalclib.Config();

		apiConfig.inFile = new FileInputStream(config.inFile);
		apiConfig.outFile = new FileOutputStream(config.outFile);
		apiConfig.printStream = System.out;

		apiConfig.execService = Executors.newFixedThreadPool(config.threadCount);

		apiConfig.precision = config.precision;
		apiConfig.useDouble = config.useDouble;
		apiConfig.useSequences = config.useSequences;
		apiConfig.threshold = config.threshold;
		//apiConfig.startPosition = config.startPosition;


		Api.calculate(apiConfig);

		apiConfig.execService.shutdown();
	}
}
