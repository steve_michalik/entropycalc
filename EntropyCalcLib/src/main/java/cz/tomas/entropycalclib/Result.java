/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

import java.util.Collection;
import java.util.List;

/**
 *
 * @author Tomas
 */
public class Result {

	public long time;

	//number of positions in each sequence
	public int positions;

	public boolean useSequence;

	//number of remaining sequences after filtering.
	public int sequences;

	//number of sequences before filtering.
	public int sequencesTotal;

	//percentage after-filtering/before-filtering
	public int sequencesPercent;

	public Collection<Interval> intervals;

	public List<GroupedSequence> groups;

	public List<PnnsCalculation> pnns;

	public long getTime() {
		return time;
	}

	public int getPositions() {
		return positions;
	}

	public boolean isUseSequence() {
		return useSequence;
	}

	public int getSequences() {
		return sequences;
	}

	public int getSequencesTotal() {
		return sequencesTotal;
	}

	public int getSequencesPercent() {
		return sequencesPercent;
	}

	public List<GroupedSequence> getGroups() {
		return groups;
	}

	public Collection<Interval> getIntervals() {
		return intervals;
	}
}

