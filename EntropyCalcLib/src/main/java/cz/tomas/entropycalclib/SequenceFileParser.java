/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 *
 * @author Tomas
 */
class SequenceFileParser extends DataParser {

	private static final String BASES1 = "ryswkmbdhvn";
	private static final String BASES2 = ".-_~?";

	private Map<Integer, Sequence> sequences;
	private List<Sequence> filteredSeqs;
	private List<GroupedSequence> groupedSequences;

	SequenceFileParser(State state) {
		super(state);
	}

	@Override
	List<PositionCalculation> parse() throws IOException {

		sequences = parseData();
		filterData();
		return buildPositions();
	}

	List<GroupedSequence> group() throws IOException {
		sequences = parseData();
		filterData();
		return groupedSequences;
	}

	List<PnnsCalculation> pnns() throws IOException {
		Map<Integer, Sequence> seqs = parseData();
		return buildPnns(seqs);
	}

	private List<PnnsCalculation> buildPnns(Map<Integer, Sequence> seqs) {

		List<PnnsCalculation> result = new ArrayList<>();

		for (int i = 0; i < seqs.get(0).getData().length(); i++) {

			int a = 0;
			int g = 0;
			int c = 0;
			int u = 0;
			int gap = 0;
			int misc = 0;

			for (Map.Entry<Integer, Sequence> entry : seqs.entrySet()) {
				Sequence sq = entry.getValue();

				char base = sq.getData().charAt(i);

				switch (base) {

					case 'a':
						a++;
						break;

					case 'g':
						g++;
						break;

					case 'c':
						c++;
						break;

					case 'u':
					case 't':
						u++;
						break;

                    default:
						if (BASES1.indexOf(base) >= 0) {
							misc++;
						}
						else if (BASES2.indexOf(base) >= 0) {
							gap++;
						}
				}
			}

			result.add(new PnnsCalculation(i + 1, a, g, c, u, gap, misc));
		}

		return result;
	}

	private Map<Integer, Sequence> parseData() throws IOException {

		state.start = System.currentTimeMillis();
		Map<Integer, Sequence> seqs;

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(state.config.inFile))) {

			state.config.printStream.println("reading input file ...");

			seqs = new HashMap<>();

			String line, nextLine;
			Integer i = 0;

			while ((line = reader.readLine()) != null) {

				if ((nextLine = reader.readLine()) == null)
					break;

				seqs.put(i, new Sequence(i+1, line, nextLine.toLowerCase()));

				i++;
			}

			state.config.printStream.println("found " + seqs.size() + " sequences.");
			state.result.sequencesTotal = seqs.size();

			if (!seqs.isEmpty()) {
				state.result.positions = seqs.get(0).getData().length();
			}
		}
		return seqs;
	}

	private void filterData() {
		if (!state.config.wholeSequence && (state.config.intervals == null || state.config.intervals.isEmpty())) {
			throw new IllegalArgumentException();
		}

		state.config.printStream.println("filtering sequences with " + state.config.threshold + "% threshold ...");

		if (state.config.wholeSequence) {
			//if the whole sequences are used, update the start and end position in config for viewing the result.
			state.result.intervals = new ArrayList<>(1);
			state.result.intervals.add(new Interval(1, state.result.positions));
		} else {
			makeSubstrings();
		}

		filteredSeqs = new ArrayList<>();
		groupedSequences = new ArrayList<>();

		GroupedSequence outgroup1 = new GroupedSequence(GroupedSequenceType.OUTGROUP1);
		GroupedSequence outgroup2 = new GroupedSequence(GroupedSequenceType.OUTGROUP2);
		GroupedSequence excluded = new GroupedSequence(GroupedSequenceType.EXCLUDED);


		Collection<Integer> toDelete = new ArrayList<>();

		for (Map.Entry<Integer, Sequence> entry : sequences.entrySet()) {
			Sequence sq = entry.getValue();
			String data = sq.getData().toLowerCase(Locale.US);

			switch (tellSeqType(data))
			{
				case EXCLUDED:
					excluded.getSequences().add(sq);
					toDelete.add(entry.getKey());
					break;

				case OUTGROUP2:
					outgroup2.getSequences().add(sq);
					toDelete.add(entry.getKey());
					break;

				case OUTGROUP1:
					outgroup1.getSequences().add(sq);
					toDelete.add(entry.getKey());
					break;
			}
		}

		for (Integer i : toDelete) {
			sequences.remove(i);
		}
		toDelete.clear();


		while (!sequences.isEmpty()) {

			Integer key = sequences.entrySet().iterator().next().getKey();
			Sequence sq = sequences.get(key);


			sequences.remove(key);
			filteredSeqs.add(sq);

			GroupedSequence gsq = new GroupedSequence();
			gsq.getSequences().add(sq);
			groupedSequences.add(gsq);


			String s1 = sq.getData();

			for (Map.Entry<Integer, Sequence> entry : sequences.entrySet()) {

				String s2 = entry.getValue().getData();

				//disabled, we are not using threshold right now.
//				int matches = 0;
//
//				//TODO je mozne skoncit driv.
//				for (int i = fromIndex; i < toIndex; i++) {
//					if (s1.charAt(i) == s2.charAt(i)) {
//						matches++;
//					}
//				}
//
//				if (100 * matches / (toIndex - fromIndex) >= state.config.threshold) {
//					sq.addWeight();
//					gsq.getSequences().add(entry.getValue());
//					toDelete.add(entry.getKey());
//				}

				if (s1.equals(s2)) {
					sq.addWeight();
					gsq.getSequences().add(entry.getValue());
					toDelete.add(entry.getKey());
				}
			}

			for (Integer i : toDelete) {
				sequences.remove(i);
			}
			toDelete.clear();
		}

		if (!outgroup1.isEmpty()) {
			groupedSequences.add(outgroup1);
		}
		if (!outgroup2.isEmpty()) {
			groupedSequences.add(outgroup2);
		}
        if (!excluded.isEmpty()) {
            groupedSequences.add(excluded);
        }

		int count = filteredSeqs.size();
		int percent = 100 * filteredSeqs.size() / state.result.sequencesTotal;

		state.config.printStream.println("done. " + count + " " + "(" + percent + "%) unique sequences remaining.");
		state.result.sequences = count;
		state.result.sequencesPercent = percent;
		state.result.time = System.currentTimeMillis() - state.start;
	}

	private GroupedSequenceType tellSeqType(String seq)
	{
		boolean inoutgr1 = false;
		boolean inoutgr2 = false;
		boolean inexcluded = true;

		for (int i = 0; i < seq.length(); i++) {
			char base = seq.charAt(i);

			if (!inoutgr2) {
				if (BASES2.indexOf(base) >= 0) {
					inoutgr2 = true;
				}
			}

			if (!inoutgr1 && !inoutgr2) {
				if (BASES1.indexOf(base) >= 0)
				{
					inoutgr1 = true;
				}
			}

			if (inexcluded) {
				if (BASES2.indexOf(base) < 0) {
					inexcluded = false;
				}
			}

			if (!inexcluded && inoutgr2) {
				break;
			}
		}

		if (inexcluded) {
			return GroupedSequenceType.EXCLUDED;
		}
		else if (inoutgr2) {
			return GroupedSequenceType.OUTGROUP2;
		}
		else if (inoutgr1) {
			return GroupedSequenceType.OUTGROUP1;
		}

		return GroupedSequenceType.NORMAL;
	}


	private void makeSubstrings() {

		int len = state.result.positions;

		Collection<Interval> intervals = new ArrayList<>(state.config.intervals.size());
		state.result.intervals = intervals;

		for (Interval i : state.config.intervals) {
			if (i.getStart() >= len) {
				continue;
			}

			Interval newInterval = new Interval(i.getStart(), Math.min(i.getEnd(), len));
			intervals.add(newInterval);
		}


		StringBuilder builder = new StringBuilder(state.result.positions);

		for (Map.Entry<Integer, Sequence> entry : sequences.entrySet()) {

			builder.setLength(0);

			for (Interval i : intervals) {
				builder.append(entry.getValue().getData().substring(i.getStart()-1, i.getEnd()));
			}

			entry.getValue().setData(builder.toString());
		}
	}

	private List<PositionCalculation> buildPositions() {

		List<PositionCalculation> result = new ArrayList<>();

		for (int i = 0; i < filteredSeqs.get(0).getData().length(); i++) {

			int a = 0;
			int g = 0;
			int c = 0;
			int u = 0;

			for (Sequence sq : filteredSeqs)

				switch (sq.getData().charAt(i)) {

					case 'a':
						a += sq.getWeight();
						break;

					case 'g':
						g += sq.getWeight();
						break;

					case 'c':
						c += sq.getWeight();
						break;

					case 'u':
					case 't':
						u += sq.getWeight();
				}

			result.add(state.factory.getCalculation(i + 1, a, g, c, u, 0));
		}

		return result;
	}

}
