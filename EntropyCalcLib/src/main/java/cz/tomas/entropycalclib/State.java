/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

/**
 *
 * @author Tomas
 */
class State {

	Config config;
	Factory factory;
	Result result;
	long start;

	State(Config config, Result result) {
		this.config = config;
		this.result = result;
		factory = new Factory(this);
	}
}
