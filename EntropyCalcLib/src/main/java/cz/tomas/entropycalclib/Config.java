/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Collection;
import java.util.concurrent.ExecutorService;

/**
 *
 * @author Tomas
 */
public class Config {

	public InputStream inFile;
	public OutputStream outFile;
	public PrintStream printStream;

	public int precision;
	public boolean useDouble;

	public ExecutorService execService;

	public boolean useSequences;
	public int threshold;
	public Collection<Interval> intervals;
	public boolean wholeSequence;
}
