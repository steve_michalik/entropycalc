/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tomas
 */
class PositionFileParser extends DataParser {

	PositionFileParser(State state) {
		super(state);
	}

	@Override
	List<PositionCalculation> parse() throws IOException {

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(state.config.inFile))) {

			List<String> lines = new ArrayList<>();

			state.config.printStream.println("reading input file ...");


			reader.readLine();
			reader.readLine();
			reader.readLine();
			reader.readLine();
			reader.readLine();

			String line;
			while ((line = reader.readLine()) != null) {
				lines.add(line);
			}


			List<PositionCalculation> calculations = new ArrayList<>(lines.size());

			for (int lineNr = 0; lineNr < lines.size(); lineNr++) {

				String[] parts = lines.get(lineNr).trim().split("\t");
				
				if (parts.length != 6) {
					System.err.println("cannot parse position of number " + lineNr + '!');
					continue;
				}

				try {
					int pos = Integer.parseInt(parts[0]);
					int a = Integer.parseInt(parts[1]);
					int g = Integer.parseInt(parts[2]);
					int c = Integer.parseInt(parts[3]);
					int u = Integer.parseInt(parts[4]);
					int gap = Integer.parseInt(parts[5]);

					calculations.add(state.factory.getCalculation(pos, a, g, c, u, gap));

				} catch (NumberFormatException ex) {
					System.err.println("cannot parse position of line number " + lineNr + '!');
				}
			}
		
			state.config.printStream.println("found " + calculations.size() + " positions.");
			state.result.positions = calculations.size();

			return calculations;
		}
	}
	
}
