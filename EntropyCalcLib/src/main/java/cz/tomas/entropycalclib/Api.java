/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

import java.io.*;
import java.util.*;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Tomas
 */
public class Api {

	private static final Logger logger = LoggerFactory.getLogger(Api.class);

	public static Result calculate(Config config) throws InterruptedException, IOException {

		Result result = new Result();
		State state = new State(config, result);

		if (config.printStream == null) {
			config.printStream = new PrintStream(new OutputStream() {

				@Override
				public void write(int b) throws IOException {
				}
			});
		}

		List<PositionCalculation> calculations = state.factory.getParser().parse();

		return calculateCommon(state, calculations);
	}

	public static Result calculateFromPnns(Config config, List<PnnsCalculation> pnns) throws InterruptedException, IOException {

		Result result = new Result();
		State state = new State(config, result);

		result.positions = pnns.size();

		if (config.printStream == null) {
			config.printStream = new PrintStream(new OutputStream() {

				@Override
				public void write(int b) throws IOException {
				}
			});
		}

		List<PositionCalculation> calculations = new ArrayList<PositionCalculation>(pnns.size());

		for (PnnsCalculation pn : pnns) {
			calculations.add(new DoublePositionCalculation(pn.position, pn.a, pn.g, pn.c, pn.u, pn.gap));
		}

		return calculateCommon(state, calculations);
	}

	private static Result calculateCommon(State state,  List<PositionCalculation> calculations) throws InterruptedException, IOException {

		Config config = state.config;
		Result result = state.result;

		config.printStream.println("calculating ...");
		long time = System.currentTimeMillis();

		config.execService.invokeAll(calculations);

		result.time = System.currentTimeMillis() - time;
		config.printStream.println("calculation finished in " + result.time + " ms.");


		config.printStream.println("writing the result ...");
		printResult(config, calculations);
		config.printStream.println("done!");

		return result;
	}

	public static Result group(Config config) throws IOException {

		Result result = new Result();
		State state = new State(config, result);

		if (config.printStream == null) {
			config.printStream = new PrintStream(new OutputStream() {

				@Override
				public void write(int b) throws IOException {
				}
			});
		}

		result.groups = new SequenceFileParser(state).group();
		return result;
	}

	public static Result pnns(Config config) throws IOException {

		long start = System.currentTimeMillis();

		Result result = new Result();
		State state = new State(config, result);

		if (config.printStream == null) {
			config.printStream = new PrintStream(new OutputStream() {

				@Override
				public void write(int b) throws IOException {
				}
			});
		}

		result.pnns = new SequenceFileParser(state).pnns();
		result.time = System.currentTimeMillis() - start;
		result.sequences = result.sequencesTotal;
		return result;
	}

	public static InputStream createChart(List<GroupedSequence> result) throws IOException, InvalidFormatException {

		if (result == null) {
			throw new IllegalArgumentException();
		}

		ByteArrayOutputStream out;

		try (InputStream in = new BufferedInputStream(Api.class.getClassLoader().getResourceAsStream("template.xlsx"))) {

			Workbook wb = WorkbookFactory.create(in);
			Sheet sheet = wb.getSheet("data");

			Collections.sort(result);

			double seqCount = result.size();
			int rowIndex = 0;
			for (GroupedSequence gs : result) {
				Row row = sheet.createRow(rowIndex++);

				if (gs.getType() == GroupedSequenceType.NORMAL) {
					row.createCell(0).setCellValue(gs.getSequences().size() / seqCount);
				} else {
					row.createCell(1).setCellValue(gs.getSequences().size() / seqCount);
				}
			}

			Name ref = wb.getName("freq");
			ref.setRefersToFormula(sheet.getSheetName() + "!$A$1:$A$" + (rowIndex - 2));
			ref = wb.getName("outgroups");
			ref.setRefersToFormula(sheet.getSheetName() + "!$B$1:$B$" + rowIndex);

			out = new ByteArrayOutputStream();
			wb.write(out);
		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	public static InputStream createStratifyResult(List<GroupedSequence> result, Collection<Interval> intervals, int totalSequences) throws IOException, InvalidFormatException {

		StringBuilder builder = new StringBuilder();

		Workbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("data");

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

		Font dataFont = wb.createFont();
		dataFont.setFontName("Courier New");

		Font outgroupFont = wb.createFont();
		outgroupFont.setColor(IndexedColors.RED.getIndex());

		short percentFormat = wb.createDataFormat().getFormat("0.000");

		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);

		CellStyle cellPercentStyle = wb.createCellStyle();
		cellPercentStyle.cloneStyleFrom(cellStyle);
		cellPercentStyle.setDataFormat(percentFormat);

		CellStyle headerStyle = wb.createCellStyle();
		headerStyle.cloneStyleFrom(cellStyle);
		headerStyle.setFont(headerFont);

		CellStyle dataStyle = wb.createCellStyle();
		dataStyle.setFont(dataFont);

		CellStyle rulerStyle = wb.createCellStyle();
		rulerStyle.cloneStyleFrom(dataStyle);
		rulerStyle.setWrapText(true);

		CellStyle outgroupStyle = wb.createCellStyle();
		outgroupStyle.cloneStyleFrom(cellStyle);
		outgroupStyle.setFont(outgroupFont);

		CellStyle outgroupPercentStyle = wb.createCellStyle();
		outgroupPercentStyle.cloneStyleFrom(outgroupStyle);
		outgroupPercentStyle.setDataFormat(percentFormat);


		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("Group Number");
		cell.setCellStyle(headerStyle);

		cell = row.createCell(1);
		cell.setCellValue("Variant Count");
		cell.setCellStyle(headerStyle);

		cell = row.createCell(2);
		cell.setCellValue("Frequency %");
		cell.setCellStyle(headerStyle);

		String[] ruler = renderRuler(intervals);

		cell = row.createCell(3);
		cell.setCellValue(ruler[0]);
		cell.setCellStyle(dataStyle);
//		row.setHeightInPoints(2*sheet.getDefaultRowHeightInPoints());

		row = sheet.createRow(1);
		cell = row.createCell(3);
		cell.setCellValue(ruler[1]);
		cell.setCellStyle(dataStyle);

		if (!result.isEmpty()) {

			Collections.sort(result);
			String[] diffStrings = diffSequences(result);

			row = sheet.createRow(2);
			cell = row.createCell(3);
			cell.setCellStyle(dataStyle);
			cell.setCellValue(result.get(0).getOneSample().toUpperCase());
			cell.setCellValue(splitToIntervals(result.get(0).getOneSample().toUpperCase(), intervals, builder, '-'));

			int index = 0;
			for (GroupedSequence gs : result) {

				row = sheet.createRow(index + 3);

				int freq = gs.getSequences().size();
				float prob = (float) freq / totalSequences * 100;

				cell = row.createCell(0);

				if (gs.getType() == GroupedSequenceType.NORMAL) {
					cell.setCellStyle(cellStyle);
					cell.setCellValue(index + 1);
				} else {
					cell.setCellStyle(outgroupStyle);
					cell.setCellValue(gs.getType().name().toLowerCase());
				}

				cell = row.createCell(1);
				cell.setCellStyle(gs.getType() == GroupedSequenceType.NORMAL ? cellStyle : outgroupStyle);
				cell.setCellValue(freq);

				cell = row.createCell(2);
				cell.setCellStyle(gs.getType() == GroupedSequenceType.NORMAL ? cellPercentStyle : outgroupPercentStyle);
				cell.setCellValue(prob);

				if (gs.getType() == GroupedSequenceType.NORMAL) {
					cell = row.createCell(3);
					cell.setCellStyle(dataStyle);
					cell.setCellValue(splitToIntervals(diffStrings[index], intervals, builder, '~'));
				}

				index++;
			}
		}

		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		wb.write(out);

        return new ByteArrayInputStream(out.toByteArray());
	}

	private static String[] diffSequences(List<GroupedSequence> groupedSequences) {
		if (groupedSequences.isEmpty()) {
			return null;
		}

		String[] result = new String[groupedSequences.size()];

		String reference = groupedSequences.get(0).getOneSample();
		StringBuilder builder = new StringBuilder(reference.length());

		int index = 0;

		for (GroupedSequence gs : groupedSequences) {

			String data = gs.getOneSample();
			for (int i = 0; i < reference.length(); i++) {

				char c = data.charAt(i);

				if (reference.charAt(i) == c) {
					builder.append('.');
				} else {
					builder.append(c);
				}
			}

			result[index] = builder.toString();
			builder.setLength(0);
			index++;
		}

		return result;
	}

	public static String[] renderRuler(Collection<Interval> intervals) {

		StringBuilder ruler = new StringBuilder();
		StringBuilder numbers = new StringBuilder();
		boolean first = true;
		int spaceUseds = 0;

		for (Interval interval : intervals) {

			if (!first) {
				ruler.append("   ");

				for (int i = 0; i < 3-spaceUseds; i++) {
					numbers.append(' ');
				}
				spaceUseds = 0;
			}

			for (int i = interval.getStart(); i <= interval.getEnd(); i++) {
				if (i % 5 == 0) {
					ruler.append('|');
				} else {
					ruler.append('.');
				}
			}

			for (int i = interval.getStart(); i <= interval.getEnd(); i++) {
				if (i % 10 == 0) {
					String nr = "" + i;

					int spaceLeft = interval.getEnd() - i + 1;

					if (nr.length() <= Math.min(8, spaceLeft + 2)) {
						numbers.append(nr);
						i += nr.length() - 1;
						spaceUseds = Math.max(nr.length() - spaceLeft, 0);
					} else {
						numbers.append(' ');
					}
				}
				else {
					numbers.append(' ');
				}
			}

			first = false;
		}

		return new String[]{ numbers.toString(), ruler.toString() };
	}

	private static String splitToIntervals(String str, Collection<Interval> intervals, StringBuilder builder, char delimeter) {

		if (intervals.size() == 1) {
			return str;
		}

		builder.setLength(0);

		int startIndex = 0;

		for (Iterator<Interval> iter = intervals.iterator(); iter.hasNext();) {
			Interval interval = iter.next();

			int endIndex = startIndex + interval.getEnd() - interval.getStart() + 1;

			builder.append(str.substring(startIndex, endIndex));

			if (iter.hasNext()) {
				builder.append(delimeter).append(delimeter).append(delimeter);
			}

			startIndex = endIndex;
		}

		return builder.toString();
	}

	private static void printResult(Config config, List<PositionCalculation> calculations) throws IOException {

//		System.out.println("writing output to " + config.outFile + " ...");
		Workbook wb = new HSSFWorkbook();
		Sheet sheet = wb.createSheet("data");

		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);

		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);

		CellStyle boldStyle = wb.createCellStyle();
		boldStyle.cloneStyleFrom(cellStyle);
		boldStyle.setFont(font);

		CellStyle boldDataFormatSyle = wb.createCellStyle();
		boldDataFormatSyle.cloneStyleFrom(boldStyle);
		boldDataFormatSyle.setDataFormat(wb.createDataFormat().getFormat("0.00"));

		//------------------- write header ---------------------
		Row row = sheet.createRow(0);

		String[] header = new String[]{ "Position", "A", "G", "C", "U", "GAP", "SUM", "Total",
										"Afreq", "Gfreq",
										"Cfreq", "Ufreq", "lnAfreq", "lnGfreq", "lnCfreq", "lnUfreq",
										"H(A)", "H(G)", "H(C)", "H(U)", "H(i)", "%Coverage" };
//		String[] header = new String[]{"Position", "A", "G", "C", "U", "GAP",
//			"H(i)"};

		CellStyle[] styles = new CellStyle[header.length];
		Arrays.fill(styles, cellStyle);

		styles[6] = boldStyle;
		styles[7] = boldStyle;
		styles[20] = boldStyle;
		styles[21] = boldDataFormatSyle;
		styles[styles.length - 1] = boldDataFormatSyle;

		for (int i = 0; i < header.length; i++) {
			Cell cell = row.createCell(i);
			cell.setCellValue(header[i]);
			cell.setCellStyle(styles[i]);
		}

			//------------------- write values -----------------
		List<Double> valueList = new ArrayList<>(header.length);

		for (int i = 0; i < calculations.size(); i++) {
			PositionCalculation calc = calculations.get(i);

			if (!calc.isOk())
				continue;

			valueList.clear();

			valueList.add((double) calc.getPosition());
			valueList.add((double) calc.getA());
			valueList.add((double) calc.getG());
			valueList.add((double) calc.getC());
			valueList.add((double) calc.getU());
			valueList.add((double) calc.getGap());
			valueList.add((double)calc.getSum());
			valueList.add((double)calc.getTotal());

			valueList.add(calc.getAfreq());
			valueList.add(calc.getGfreq());
			valueList.add(calc.getCfreq());
			valueList.add(calc.getUfreq());
			valueList.add(calc.getLnAfreq());
			valueList.add(calc.getLnGfreq());
			valueList.add(calc.getLnCfreq());
			valueList.add(calc.getLnUfreq());
			valueList.add(calc.getAEntropy());
			valueList.add(calc.getGEntropy());
			valueList.add(calc.getCEntropy());
			valueList.add(calc.getUEntropy());
			valueList.add(calc.getEntropy());
			valueList.add(calc.getCoverage());

			row = sheet.createRow(i + 2);

			for (int j = 0; j < valueList.size(); j++) {
				Cell cell = row.createCell(j);
				cell.setCellValue(valueList.get(j));
				cell.setCellStyle(styles[j]);
			}
		}

		sheet.autoSizeColumn(21);
		wb.write(config.outFile);
	}

	public static InputStream createPnnsResult(List<PnnsCalculation> result) throws IOException {

		Workbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("data");

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);

		CellStyle headerStyle = wb.createCellStyle();
		headerStyle.cloneStyleFrom(cellStyle);
		headerStyle.setFont(headerFont);


		Row row = sheet.createRow(0);

		Cell cell = row.createCell(0);
		cell.setCellValue("Position");
		cell.setCellStyle(cellStyle);

		cell = row.createCell(1);
		cell.setCellValue("A");
		cell.setCellStyle(cellStyle);
		cell = row.createCell(2);
		cell.setCellValue("G");
		cell.setCellStyle(cellStyle);
		cell = row.createCell(3);
		cell.setCellValue("C");
		cell.setCellStyle(cellStyle);
		cell = row.createCell(4);
		cell.setCellValue("T");
		cell.setCellStyle(cellStyle);
		cell = row.createCell(5);
		cell.setCellValue("GAP");
		cell.setCellStyle(cellStyle);

		cell = row.createCell(6);
		cell.setCellValue("Misc");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(7);
		cell.setCellValue("SUM");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(8);
		cell.setCellValue("Total");
		cell.setCellStyle(headerStyle);


        int index = 1;
        for (PnnsCalculation pnns : result) {

            row = sheet.createRow(index);

            cell = row.createCell(0);
			cell.setCellValue(pnns.position);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(1);
			cell.setCellValue(pnns.a);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(2);
			cell.setCellValue(pnns.g);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(3);
			cell.setCellValue(pnns.c);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(4);
			cell.setCellValue(pnns.u);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(5);
			cell.setCellValue(pnns.gap);
			cell.setCellStyle(cellStyle);

			cell = row.createCell(6);
			cell.setCellValue(pnns.misc);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(7);
			cell.setCellValue(pnns.sum);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(8);
			cell.setCellValue(pnns.total);
			cell.setCellStyle(cellStyle);

            index++;
        }

//		sheet.autoSizeColumn(0);
//		sheet.autoSizeColumn(1);
//		sheet.autoSizeColumn(2);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		wb.write(out);

		return new ByteArrayInputStream(out.toByteArray());
	}
}
