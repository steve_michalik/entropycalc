/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

class DoublePositionCalculation extends PositionCalculation {

	private double aFreq;
	private double gFreq;
	private double cFreq;
	private double uFreq;

	private double lnAfreq;
	private double lnGfreq;
	private double lnCfreq;
	private double lnUfreq;

	private double aEntropy;
	private double gEntropy;
	private double cEntropy;
	private double uEntropy;

	private double entropy;


	DoublePositionCalculation(int pos, int a, int g, int c, int u, int gap) {
		super(pos, a, g, c, u, gap);
	}

	@Override
	public Boolean call() throws Exception {
		
		super.call();

		aFreq = (double) a / sum;
		gFreq = (double) g / sum;
		cFreq = (double) c / sum;
		uFreq = (double) u / sum;

		lnAfreq = log(aFreq);
		lnGfreq = log(gFreq);
		lnCfreq = log(cFreq);
		lnUfreq = log(uFreq);

		aEntropy = -aFreq*lnAfreq;
		gEntropy = -gFreq*lnGfreq;
		cEntropy = -cFreq*lnCfreq;
		uEntropy = -uFreq*lnUfreq;

		entropy = aEntropy + gEntropy + cEntropy + uEntropy;

		ok = true;
		return true;
	}

	private double log(double x) {
		return x == 0 ? 0 : Math.log(x);
	}
	
	@Override
	double getAfreq() {
		return aFreq;
	}

	@Override
	double getGfreq() {
		return gFreq;
	}

	@Override
	double getCfreq() {
		return cFreq;
	}

	@Override
	double getUfreq() {
		return uFreq;
	}

	@Override
	double getLnAfreq() {
		return lnAfreq;
	}

	@Override
	double getLnGfreq() {
		return lnGfreq;
	}

	@Override
	double getLnCfreq() {
		return lnCfreq;
	}

	@Override
	double getLnUfreq() {
		return lnUfreq;
	}

	@Override
	double getAEntropy() {
		return aEntropy;
	}

	@Override
	double getGEntropy() {
		return gEntropy;
	}

	@Override
	double getCEntropy() {
		return cEntropy;
	}

	@Override
	double getUEntropy() {
		return uEntropy;
	}

	@Override
	double getEntropy() {
		return entropy;
	}
}
