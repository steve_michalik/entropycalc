package cz.tomas.entropycalclib;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomas on 16/12/2015.
 */
public class GroupedSequence implements Comparable<GroupedSequence> {

    private List<Sequence> sequences = new ArrayList<>();

    private GroupedSequenceType type = GroupedSequenceType.NORMAL;


    public GroupedSequence() {
    }

    public GroupedSequence(GroupedSequenceType type) {
        this.type = type;
    }

    public List<Sequence> getSequences() {
        return sequences;
    }

    public GroupedSequenceType getType() {
        return type;
    }

    public int getPriority() {
        return type.ordinal();
    }

    public boolean isEmpty() {
        return sequences.isEmpty();
    }

    public String getOneSample() {
        if (sequences.isEmpty()) {
            return null;
        }

        return sequences.get(0).getData();
    }

    @Override
    public int compareTo(GroupedSequence o) {
        if (this.getPriority() != o.getPriority()) {
            return this.getPriority() - o.getPriority();
        }
        return o.getSequences().size() - this.getSequences().size();
    }
}
