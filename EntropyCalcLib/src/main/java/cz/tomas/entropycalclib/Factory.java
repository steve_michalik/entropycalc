/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;


/**
 *
 * @author Tomas
 */
class Factory {

	private final State state;


	Factory(State state) {
		this.state = state;
	}

	PositionCalculation getCalculation(int pos, int a, int g, int c, int u, int gap) {

		if (state.config.useDouble)
			return new DoublePositionCalculation(pos, a, g, c, u, gap);

		return new ApfloatPositionCalculation(pos, a, g, c, u, gap, state.config.precision);
		
	}

	DataParser getParser() {

		if (state.config.useSequences)
			return new SequenceFileParser(state);

		return new PositionFileParser(state);
	}





	
}
