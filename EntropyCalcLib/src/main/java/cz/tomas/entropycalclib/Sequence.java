/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

/**
 *
 * @author Tomas
 */
public class Sequence {

	private int number;
	private int weight;
	private String meta;
	private String data;

	Sequence(int number, String meta, String data) {
		weight = 1;
		this.number = number;
		this.meta = meta;
		this.data = data;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void addWeight() {
		weight++;
	}
}
