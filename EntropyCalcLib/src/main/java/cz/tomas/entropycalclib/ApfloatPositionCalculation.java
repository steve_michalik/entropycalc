/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

/**
 *
 * @author Tomas
 */
class ApfloatPositionCalculation extends PositionCalculation {
	private int precision;

	private Apfloat Afreq;
	private Apfloat Gfreq;
	private Apfloat Cfreq;
	private Apfloat Ufreq;

	private Apfloat lnAfreq;
	private Apfloat lnGfreq;
	private Apfloat lnCfreq;
	private Apfloat lnUfreq;

	private Apfloat AEntropy;
	private Apfloat GEntropy;
	private Apfloat CEntropy;
	private Apfloat UEntropy;

	private Apfloat entropy;

	ApfloatPositionCalculation(int pos, int a, int g, int c, int u, int gap, int precision) {
		super(pos, a, g, c, u, gap);
		this.precision = precision;
	}

	@Override
	public Boolean call() {
		try {
			super.call();

			Apfloat sumApf = new Apfloat(sum, precision);

			Afreq = new Apfloat(a, precision).divide(sumApf);
			Gfreq = new Apfloat(g, precision).divide(sumApf);
			Cfreq = new Apfloat(c, precision).divide(sumApf);
			Ufreq = new Apfloat(u, precision).divide(sumApf);
			lnAfreq = log(Afreq);
			lnGfreq = log(Gfreq);
			lnCfreq = log(Cfreq);
			lnUfreq = log(Ufreq);
			AEntropy = Afreq.multiply(lnAfreq).negate();
			GEntropy = Gfreq.multiply(lnGfreq).negate();
			CEntropy = Cfreq.multiply(lnCfreq).negate();
			UEntropy = Ufreq.multiply(lnUfreq).negate();
			entropy = AEntropy.add(GEntropy).add(CEntropy).add(UEntropy);
		} catch (Exception e) {
			System.err.println("Position " + position + " calculation error, reason: " + e.getMessage());
			return Boolean.FALSE;
		}
		ok = true;
		return Boolean.TRUE;
	}
	
	private Apfloat log(Apfloat x){
		if (x.equalDigits(Apfloat.ZERO) >= precision) {
			return Apfloat.ZERO;
		}

		return ApfloatMath.log(x);
	}

	@Override
	double getAfreq() {
		return Afreq.doubleValue();
	}

	@Override
	double getGfreq() {
		return Gfreq.doubleValue();
	}

	@Override
	double getCfreq() {
		return Cfreq.doubleValue();
	}

	@Override
	double getUfreq() {
		return Ufreq.doubleValue();
	}

	@Override
	double getLnAfreq() {
		return lnAfreq.doubleValue();
	}

	@Override
	double getLnGfreq() {
		return lnGfreq.doubleValue();
	}

	@Override
	double getLnCfreq() {
		return lnCfreq.doubleValue();
	}

	@Override
	double getLnUfreq() {
		return lnUfreq.doubleValue();
	}

	@Override
	double getAEntropy() {
		return AEntropy.doubleValue();
	}

	@Override
	double getGEntropy() {
		return GEntropy.doubleValue();
	}

	@Override
	double getCEntropy() {
		return CEntropy.doubleValue();
	}

	@Override
	double getUEntropy() {
		return UEntropy.doubleValue();
	}

	@Override
	double getEntropy() {
		return entropy.doubleValue();
	}

}
