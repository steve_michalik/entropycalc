package cz.tomas.entropycalclib;

/**
 * Created by Tomas on 07/04/2016.
 */
public enum GroupedSequenceType {
    NORMAL,
    OUTGROUP1,
    OUTGROUP2,
    EXCLUDED,
}
