/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;


import java.util.concurrent.Callable;

public class PnnsCalculation {

	protected int position;

	protected int a;
	protected int g;
	protected int c;
	protected int u;
	protected int gap;
	protected int misc;
	protected int sum;
	protected int total;

	PnnsCalculation(int position, int a, int g, int c, int u, int gap, int misc) {
		this.position = position;
		this.a = a;
		this.g = g;
		this.c = c;
		this.u = u;
		this.gap = gap;
		this.misc = misc;

		sum = a + g + c + u;
		total = gap + misc + sum;
	}

	int getPosition() {
		return position;
	}

	int getA() {
		return a;
	}

	int getG() {
		return g;
	}

	int getC() {
		return c;
	}

	int getU() {
		return u;
	}

	int getGap() {
		return gap;
	}

	int getSum() {
		return sum;
	}

	int getTotal() {
		return total;
	}
}
