/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

import java.io.IOException;
import java.util.List;

/**
 *
 * @author Tomas
 */
abstract class DataParser {

	protected State state;

	DataParser(State state) {
		this.state = state;
	}

	abstract List<PositionCalculation> parse() throws IOException;
	
}
