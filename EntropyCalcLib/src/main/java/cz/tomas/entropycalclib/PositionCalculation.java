/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.tomas.entropycalclib;

import java.util.concurrent.Callable;

abstract class PositionCalculation implements Callable<Boolean> {

	protected boolean ok;
	protected int position;

	protected int a;
	protected int g;
	protected int c;
	protected int u;
	protected int gap;
	protected int sum;
	protected int total;

	protected double coverage;

	PositionCalculation(int position, int a, int g, int c, int u, int gap) {
		this.position = position;
		this.a = a;
		this.g = g;
		this.c = c;
		this.u = u;
		this.gap = gap;
	}

	@Override
	public Boolean call() throws Exception {
		sum = a + g + c + u;
		total = gap + sum;
		coverage = (double) sum / total * 100;

		return Boolean.TRUE;
	}

	boolean isOk() {
		return ok;
	}

	int getPosition() {
		return position;
	}

	int getA() {
		return a;
	}

	int getG() {
		return g;
	}

	int getC() {
		return c;
	}

	int getU() {
		return u;
	}

	int getGap() {
		return gap;
	}

	int getSum() {
		return sum;
	}

	int getTotal() {
		return total;
	}

	double getCoverage() {
		return coverage;
	}

	abstract double getAfreq();
	abstract double getGfreq();
	abstract double getCfreq();
	abstract double getUfreq();

	abstract double getLnAfreq();
	abstract double getLnGfreq();
	abstract double getLnCfreq();
	abstract double getLnUfreq();

	abstract double getAEntropy();
	abstract double getGEntropy();
	abstract double getCEntropy();
	abstract double getUEntropy();

	abstract double getEntropy();
}
